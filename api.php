<?php
header("Content-Type:application/json");
require "data.php";

if(!empty($_GET['name']))
{
	$name=$_GET['name'];
	$price = get_price($name);
	$lang = getenv('LANGUAGE');
	
	if(empty($price))
	{
		if($lang == "spanish") {
			response(200,"No se encontro el producto $name ",NULL);
		} else {
			response(200,"Product $name Not Found",NULL);
		}
	}
	else
	{
		if($lang == "spanish") {
			response(200,"Si se encontro el producto $name ",$price);
		} else {
			response(200,"Product $name Found",$price);
		}
	}
	
}
else
{
	response(400,"Invalid Request",NULL);
}

function response($status,$status_message,$data)
{
	header("HTTP/1.1 ".$status);
	
	$response['status']=$status;
	$response['status_message']=$status_message;
	$response['data']=$data;
	
	$json_response = json_encode($response);
	echo $json_response;
}