<?php

function get_price($name)
{

	$lang = getenv('LANGUAGE');

	$products = [
		"book"=>20,
		"pen"=>10,
		"pencil"=>5
	];

	$products_spanish = [
		"libro"=>20,
		"boligrafo"=>10,
		"lapiz"=>5
	];
	
	if ($lang == "spanish") {

		foreach($products_spanish as $product=>$price) {
			if($product==$name) {
				return $price;
				break;
			}
		}

	} else {

		foreach($products as $product=>$price) {
			if($product==$name) {
				return $price;
				break;
			}
		}

	}

}